# Projet infra VPN

## Getting started

Le projet est un VPN et il à été réalisé pour un serveur tournant sur debian 11 et il est recommander de l'utiliser sur une machine avec cette même configuration

## Installer et configurer Wireshark

### Installation

-commencer d'abors par mettre a jour les packet

```console
sudo apt update && sudo apt upgrade
```

-installer wireshark

```console
sudo apt install wireshark
```

### Configuration

-pour la configuration il suffit de lancer le scripts "setupWireshark" dans ./scripts

```console
sudo ./setupWireshark
```

-pour finir, il faudra de-commenter une ligne dans sysctl.conf

```console
sudo nano /etc/sysctl.conf

net.ipv4.ip_forward=1  <-- de-commenter cette ligne
```

-wireshark et maintenant configurer

## Installer et configuer le bot Discord

-le bot discord n'est qu'un outil pour manipuler le VPN plus simple pour manipuler le VPN, il n'est pas obligatoire pour son bon fonctionnement, mais les manipulations a effectuer pour utiliser le VPN sans le bot ne seront pas traiter ici

### Installation

-le bot a été créer avec [discord.js](https://discord.js.org/#/), il faut donc avant tout installer nodejs

- il faut d'abord faire cette commande pour installer nvm, qui nous permettra de choisir la version de node que l'on veut installer

```console
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.2/install.sh | bash
```

- ensuite il faut installer node depuis nvm

```console
nvm install --lts
```

-il faudra créer un serveur discord auquelle seul vous et ceux qui autont le droit de manipuler le VPN auront accés

-il faut ensuite créer le bot, en allant sur [discord.com/developpers](https://discord.com/developers/applications) (il faut se connecter avec son compte discord)

- il faut créer une nouvelle application et lui donner un nom

- il faut récupererle clientID et le token du bot et les stocker dans un endroit sur

- le token est disponible dans la section "Bot"

- le clientId est disponible dans la section "General Information"

- il faut ensuite générer une url pour inviter le bot sur notre serveur dans la section "OAuth2/URL Generator"

- selectionner bot et applications.commands dans le block "scopes", et Administrator dans le block "Bot Permissions"

- il ne suffit plus qu'as se rendre sur l'url générer et inviter le bot sur notre serveur

-maintenant, il faut créer le fichier .env pour le projet

```js
BOT_TOKEN=YOUR_BOT_TOKEN
CLIENT_ID=YOUR_BOT_CLIENT_ID
GUILD_ID=YOUR_GUILD_ID;
PUBLIC_KEY=YOU_PUBLIC_KEY;
SERVER_ADDRESS=YOU_SERVER_PUBLIC_ADDRESS;
```

- "YOU_PUBLIC_KEY" est trouvable avec :

```console
sudo cat /etc/wireguard/publickey
```

- "YOUR_GUILD_ID" est l'id de votre serveur discord, qui est trouvable en faisant clique doir + copier l'identifiant sur votre serveur (il faut activer les options developper dans les paramètres de discord)

-apres ca, il faut installer les dependances node du projet

```console
npm install
```

-il suffit desormais de lancer le bot

```console
npm run start:prod
```

## Uilisation du bot Discord

-le bot discord dispose de 6 commandes qui serve a gérer le VPN est le bot, toutes les commandes osnt disponibles en commançant u nmessage par "/"

- "AddUser" : permet de rajouter un utilisateur pouvant utiliser le vpn avec sa clé publique et un nom

```console
/AddUser name:string public_key:string
```

- "DeleteUser" : permet de supprimer un utilisateur pouvant utiliser le vpn avec son nom

```console
/DeleteUser name:string
```

- "ListUser" : permet d'afficher tout les noms des utilisateurs enregistrer sur le VPN

```console
/ListUser
```

- "StartVpn" : permet d'allumer le vpn s'il n'est pas déjà en marche

```console
/StartVpn
```

- "CloseVpn" : permet d'éteindre le vpn s'il n'est pas déjà éteint

```console
/CloseVpn
```

- "Exit" : permet d'éteindre le bot discord

```console
/Exit
```