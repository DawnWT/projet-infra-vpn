import 'dotenv/config';
import { BotClient } from "./structures/Client";

export const client = new BotClient({ intents: ['GUILDS', 'GUILD_MESSAGES'] })
client.start()