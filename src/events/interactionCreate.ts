import { CommandInteraction, CommandInteractionOptionResolver } from "discord.js";
import { client } from "../app";
import { Event } from "../structures/Event";

export default new Event('interactionCreate', (interaction) => {
  if (!interaction.isCommand()) return
  const { commandName } = interaction
  client.commands.get(commandName)?.run({ args: interaction.options as CommandInteractionOptionResolver, client, interaction })
})