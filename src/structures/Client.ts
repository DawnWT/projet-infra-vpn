import { exec } from "child_process";
import { ApplicationCommand, ApplicationCommandDataResolvable, Client, ClientEvents, ClientOptions, Collection } from "discord.js";
import fs from 'fs/promises';
import { glob } from "glob";
import { promisify } from "util";
import { RegisterCommandsOptions } from "../typings/Client";
import { CommandType } from "../typings/Command";
import { Db, DbUser } from "../typings/Db";
import { Event } from "./Event";
const pGlob = promisify(glob)
const pExec = promisify(exec)

export class BotClient extends Client {
  commands: Collection<string, CommandType> = new Collection()
  isVpnOn = true

  constructor(options: ClientOptions) {
    super(options)
  }

  start() {
    this.registerModules()
    this.login(process.env.BOT_TOKEN)
  }

  async StartVpn() {
    await pExec('sudo wg-quick up wg0')
    this.isVpnOn = true
  }

  async CloseVpn() {
    await pExec('sudo wg-quick down wg0')
    this.isVpnOn = false
  }

  async importFile(path: string) {
    return (await import(path))?.default
  }

  async registerCommands({ commands, guildId }: RegisterCommandsOptions) {
    await this.application?.commands.set([])
    if (guildId) {
      await this.guilds.cache.get(guildId)?.commands.set(commands)
      // console.log(`Registering commands to ${guildId}`);
    } else {
      await this.application?.commands.set(commands)
      // console.log(a);
      // console.log('Registering global commands');
    }
  }

  async registerModules() {
    // Commands
    const slashCommands: ApplicationCommandDataResolvable[] = []
    const commandFiles = await pGlob(`${__dirname}/../commands/*{.ts,.js}`)
    // console.log({ commandFiles })
    for (const filePath of commandFiles) {
      const command: CommandType = await this.importFile(filePath)
      if (!command.name) continue
      // console.log(command);


      this.commands.set(command.name, command)
      slashCommands.push(command)
    }

    this.on('ready', async () => {
      // await this.startVpn();
      this.registerCommands({
        commands: slashCommands,
        guildId: process.env.GUILD_ID
      });
    })

    // Events
    const eventFiles = await pGlob(`${__dirname}/../events/*{.ts,.js}`)
    for (const filePath of eventFiles) {
      const event: Event<keyof ClientEvents> = await this.importFile(filePath)
      this.on(event.event, event.run)
    }
  }

  async GetOneIp(): Promise<number> {
    const dbBuffer = await fs.readFile('./db.json')
    const db: Db = JSON.parse(dbBuffer.toString())

    return db.availableIp[0]
  }

  async GetUserKey(name: string): Promise<string> {
    const dbBuffer = await fs.readFile('./db.json')
    const db: Db = JSON.parse(dbBuffer.toString())

    const key = db.users.filter(w => w.name === name)[0].key

    return key || 'error1'
  }

  async AddInDb(user: DbUser): Promise<number> {
    const dbBuffer = await fs.readFile('./db.json')
    const db: Db = JSON.parse(dbBuffer.toString())

    if (user.key === 'error1') return -1
    if (db.users.filter(w => w.name === user.name).length > 0) return -2

    user.ip = db.availableIp[0]
    db.users.push(user)
    db.availableIp.splice(0, 1)

    await fs.writeFile('./db.json', JSON.stringify(db))

    return 0
  }

  async DeleteFromDb(name: string) {
    const dbBuffer = await fs.readFile('./db.json')
    const db: Db = JSON.parse(dbBuffer.toString())

    const user = db.users.filter(w => w.name === name)[0]

    db.users = db.users.filter(w => w.name !== name)
    db.availableIp.push(user.ip as number)

    await fs.writeFile('./db.json', JSON.stringify(db))
  }

  async GetAllUsers(): Promise<DbUser[]> {
    const dbBuffer = await fs.readFile('./db.json')
    const db: Db = JSON.parse(dbBuffer.toString())

    return db.users
  }
}
