import { ChatInputApplicationCommandData, CommandInteraction, CommandInteractionOptionResolver, PermissionResolvable } from "discord.js";
import { BotClient } from "../structures/Client";

interface RunOptions {
  client: BotClient,
  interaction: CommandInteraction,
  args: CommandInteractionOptionResolver
}

type RunFunction = (options: RunOptions) => Promise<void>

export type CommandType = {
  userPermissions?: PermissionResolvable[],
  cooldown?: number,
  run: RunFunction
} & ChatInputApplicationCommandData
