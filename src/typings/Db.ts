export type DbUser = {
  name: string,
  key: string,
  ip: number
}
export type Db = {
  users: DbUser[],
  availableIp: number[]
}