import { exec } from "child_process";
import { promisify } from "util";
import { Command } from "../structures/Command";
const pExec = promisify(exec)

export default new Command({
  name: 'add-user',
  description: 'Add one user to the VPN',
  run: async ({ interaction, client }) => {
    await interaction.deferReply()
    const name = interaction.options.getString('name'),
      key = interaction.options.getString('public-key')

    if (!name || !key) {
      await interaction.followUp('missing name or publick key')
      return
    }

    const ip = await client.GetOneIp()

    let res = {
      stdout: '',
      stderr: ''
    }

    try {
      res = await pExec(`sudo ./scripts/AddUser.sh ${key} ${ip}`)
    } catch (err: any) {
      await interaction.followUp(err.message)
      await client.DeleteFromDb(name)
      return
    }

    if (res.stderr) {
      await interaction.followUp('[error in the script: \'AddUser\']')
      await interaction.followUp(res.stderr)
      return
    }

    const status = await client.AddInDb({ name, key, ip })

    if (status === -1) {
      await interaction.followUp('your key isn\'t usable')
      return
    }
    if (status === -2) {
      await interaction.followUp('this user already is in the VPN')
      return
    }

    await interaction.followUp(`${name} has been added to the db`)

    await interaction.followUp('For windows:\nOpen wireguard, add a blank tunnel, add this after \'PrivateKey = YOUR_PRIVATE_KEY\'')
    await interaction.followUp(`Address = 10.0.0.${ip}/24

    [Peer]
    PublicKey = ${process.env.PUBLIC_KEY}
    Endpoint = ${process.env.SERVER_ADDRESS}:51820
    AllowedIPs = 0.0.0.0/0`)

    await interaction.followUp('For linux/mac:\nadd the following content to /etc/wireguard/wg0.conf')
    await interaction.followUp(`[Interface]
    PrivateKey = YOUR_PRIVATE_KEY
    Address = 10.0.0.${ip}/24
    
    
    [Peer]
    PublicKey = ${process.env.PUBLIC_KEY}
    Endpoint = ${process.env.SERVER_ADDRESS}:51820
    AllowedIPs = 0.0.0.0/0
    `)
  },
  options: [{ name: 'name', description: 'name of the user', type: 'STRING', required: true }, { name: 'public-key', description: 'Public Key of the user', type: 'STRING', required: true }]
})