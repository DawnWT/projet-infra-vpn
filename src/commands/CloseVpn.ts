import { Command } from "../structures/Command";

export default new Command({
  name: 'close-vpn',
  description: 'close the VPN',
  run: async ({ interaction, client }) => {
    await interaction.deferReply()

    if (client.isVpnOn) {
      try {
        await client.CloseVpn()
      } catch (err: any) {
        await interaction.followUp('[error]')
        await interaction.followUp(err.message)

        return
      }
      await interaction.followUp('VPN off')
    } else {
      await interaction.followUp('VPN is already stopped')
    }
  }
})