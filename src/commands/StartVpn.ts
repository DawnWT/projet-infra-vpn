import { Command } from "../structures/Command";

export default new Command({
  name: 'start-vpn',
  description: 'start the VPN',
  run: async ({ interaction, client }) => {
    await interaction.deferReply()

    if (client.isVpnOn) {
      await interaction.followUp('VPN is already running')
    } else {
      try {
        await client.StartVpn()
      } catch (err: any) {
        await interaction.followUp('[error]')
        await interaction.followUp(err.message)

        return
      }
      await interaction.followUp('VPN on')
    }
  }
})