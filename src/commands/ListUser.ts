import fs from 'fs/promises';
import { Command } from "../structures/Command";

export default new Command({
  name: 'list-user',
  description: 'Display a list of all users',
  run: async ({ interaction, client }) => {
    await interaction.deferReply()

    const users = await client.GetAllUsers()

    await interaction.followUp(`List of users:\n${users.map(u => u.name).join('\n')}`)
  }
})