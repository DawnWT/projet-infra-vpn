import { exec } from "child_process";
import { promisify } from "util";
import { Command } from "../structures/Command";
const pExec = promisify(exec)

export default new Command({
  name: 'delete-user',
  description: 'Delete one user from the VPN',
  run: async ({ interaction, client }) => {
    await interaction.deferReply()
    const name = interaction.options.getString('name')

    if (!name) {
      await interaction.followUp('missing name')
      return
    }

    const key = await client.GetUserKey(name)

    if (key === 'eror1') {
      await interaction.followUp('this user doesn\'t exist')
      return
    }

    let res = {
      stdout: '',
      stderr: ''
    }

    try {
      res = await pExec(`sudo ./scripts/DeleteUser.sh ${key}`)
    } catch (err: any) {
      await interaction.followUp(err.message)
      return
    }

    if (res.stderr) {
      await interaction.followUp('[error in the script: \'DeleteUser\']')
      await interaction.followUp(res.stderr)
      return
    }

    await client.DeleteFromDb(name)

    await interaction.followUp(`${name} has been removed to the db`)
  },
  options: [{ name: 'name', description: 'name of the user', type: 'STRING', required: true }]
})