import { Command } from "../structures/Command";

export default new Command({
  name: 'exit',
  description: 'Stop the bot',
  run: async ({ interaction }) => {
    await interaction.reply('Bot Stopped!')
    process.exit(0)
  }
})