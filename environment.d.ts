declare global {
  namespace NodeJS {
    interface ProcessEnv {
      BOT_TOKEN: string;
      CLIENT_ID: string;
      ENVIRONMENT: 'DEV' | 'PROD' | 'DEBUG';
      GUILD_ID: string;
      PUBLIC_KEY: string;
      SERVER_ADDRESS: string;
    }
  }
}

export { };

