wg genkey | sudo tee /etc/wireguard/privatekey | wg pubkey | sudo tee /etc/wireguard/publickey

pubKey = cat /etc/wireguard/publickey
privKey = cat /etc/wireguard/privatekey

res = ip -o -4 route show to default | awk '{print $5}'

setup = "[Interface]
Address = 10.0.0.1/24
SaveConfig = true
ListenPort = 51820
PrivateKey = $privKey
PostUp = iptables -A FORWARD -i %i -j ACCEPT; iptables -t nat -A POSTROUTING -o $res -j MASQUERADE
PostDown = iptables -D FORWARD -i %i -j ACCEPT; iptables -t nat -D POSTROUTING -o $res -j MASQUERADE"

touch /etc/wireguard/wg0.conf
/etc/wireguard/wg0.conf > setup

chmod 600 /etc/wireguard/{privatekey,wg0.conf}