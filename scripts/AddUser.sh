if [ $# -eq 2 ]
then
  wg set wg0 peer $1 allowed-ips 10.0.0.$2
else
  echo "missing arguments"
fi
